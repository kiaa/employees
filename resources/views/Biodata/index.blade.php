@include('base.header')
<section id="main-content">
    <section class="content-wrapper">
            
        <div class="col-lg-12">
            <div class="card">
                <div class="box box-primary">
                <div class="card-header">
                    <strong>Biodata</strong>
                </div>
            <div class="card-body card-block">
                <form action="/bio" method="POST" enctype="multipart/from-data" class="form-horizontal">
                @csrf
                  <div class="box-body">
                  {{-- get session flash --}}
                  @if(Session::has('message'))
                  <h4><strong>{{session::get('message')}}</strong></h4>
                @endif

                {{-- get validation --}}
                 @if (count($errors) > 0)
                  <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                   </ul>
                   </div>
                @endif
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">name</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter name ..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">address</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter address ..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">phone_number</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter phone_number ..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">email</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter email ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">date_of_birth</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter date_of_birth ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">religion</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter religion ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Edu_id</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter Edu_id ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Job_id</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter Job_id ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Training_id</label></div>
                        <div class="col-12 col-md-9"><input type="text" name="name" placeholder="Enter Training_id ..." class="form-control"></div>
                    </div>

                     <div class="row form-group">
                        <div class="col col-md-3"><label for="file-multiple-input" class=" form-control-label">Image</label></div>
                        <div class="col-12 col-md-9"><input type="file" name="Image" class="form-control-file"></div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form>
            </div>           
            </div>
        </div>
    </section> 
</section> 

@include('base.footer')