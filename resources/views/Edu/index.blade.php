@include('base.header')
<section id="main-content">
    <section class="content-wrapper">
            
        <div class="col-lg-12">
            <div class="card">
                <div class="box box-primary">
                <div class="card-header">
                    <strong>Last Education</strong>
                </div>
            <div class="card-body card-block">
                <form role="form" action="/edu" method="POST" enctype="multipart/from-data" class="form-horizontal">
                @csrf
                  <div class="box-body">
                  {{-- get session flash --}}
                  @if(Session::has('message'))
                  <h4><strong>{{session::get('message')}}</strong></h4>
                @endif

                {{-- get validation --}}
                 @if (count($errors) > 0)
                  <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                   </ul>
                   </div>
                @endif
                    <div class="row form-group">
                        <div class="col col-md-3"><label>name</label></div>
                        <div class="col-12 col-md-9">
                            <input type="text" name="name" placeholder="Enter name ..." class="form-control"></div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  
                </form>
            </div>
            </div>
        </div>
    </section> 

    
</section> 

@include('base.footer')