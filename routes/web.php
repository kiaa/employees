<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('template'); });

Route::get('/edu','\App\Http\Controllers\EducationController@index');
Route::post('/edu', '\App\Http\Controllers\EducationController@store');

// Route::post('edu',[EducationController::class,'store']);
// Route::get('edu',[EducationController::class,'index']);

Route::get('job','App\Http\Controllers\JobController@index');
Route::post('job','JobController@store');

Route::get('training','App\Http\Controllers\TrainingController@index');
Route::post('training','TrainingController@store');

Route::get('bio','App\Http\Controllers\BiodataController@index');
Route::post('bio','BiodataController@store');