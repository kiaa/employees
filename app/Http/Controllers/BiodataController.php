<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bio;
use Illuminate\Support\Facades\Session;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bio = Bio::all();

        return view ('Biodata.index',compact('Bio', 'Education', 'job', 'training'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'date_of_birth' => 'required',
            'religion' => 'required',
            'image' => 'required',
            'Edu_id' => 'required',
            'Job_id' => 'required',
            'Training_id' => 'required'
    	]);

    	try {
            $bio = new Bio();
            
             $imageName = time().'.'.request()->image->getClientOriginalExtension();
             request()->image->move(public_path('images'), $imageName);

            $bio->name = $request->name;
            $bio->address = $request->address;
            $bio->phone_number = $request->phone_number;
            $bio->email = $request->email;
            $bio->date_of_birth = $request->date_of_birth;
            $bio->religion = $request->religion;
            $bio->image = $request->imageName;
            $bio->Edu_id = $request->Edu_id;
            $bio->Job_id = $request->Job_id;
            $bio->Training_id = $request->Training_id;
    		$bio->save();

    		Session::flash('message','data berhasil di simpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message','Data tidak berhasil disimpan');
    		return redirect()->back();
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
