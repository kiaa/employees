<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\job;
use Illuminate\Support\Facades\Session;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $job = job::all();

        return view ('Job.index',compact('job'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'field_of_work' => 'required',
            'position' => 'required',
            'years' => 'required'
    	]);

    	try {
    		$job = new job();
            $job->name = $request->name;
            $job->address = $request->address;
            $job->field_of_work = $request->field_of_work;
            $job->position = $request->position;
            $job->years = $request->years;
    		$job->save();

    		Session::flash('message','data berhasil di simpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message','Data tidak berhasil disimpan');
    		return redirect()->back();
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
