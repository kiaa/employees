<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\training;
use Illuminate\Support\Facades\Session;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $training = training::all();

        return view ('Training.index',compact('training'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'administration' => 'required',
            'year' => 'required',
            'place' => 'required'
    	]);

    	try {
    		$training = new training();
            $training->name = $request->name;
            $training->administration = $request->administration;
            $training->year = $request->year;
            $training->place = $request->place;
    		$training->save();

    		Session::flash('message','data berhasil di simpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message','Data tidak berhasil disimpan');
    		return redirect()->back();
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
