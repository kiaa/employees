<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Education;
use Illuminate\Support\Facades\Session;

class EducationController extends Controller
{
    public function index()
    {
        $edu = Education::all();

        return view ('Edu.index',compact('edu'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'name' => 'required'
    	]);

    	try {
    		$edu = new Education();
    		$edu->name = $request->name;
    		$edu->save();

    		Session::flash('message','data berhasil di simpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message','Data tidak berhasil disimpan');
    		return redirect()->back();
    	}
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
