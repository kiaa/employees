<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bio extends Model
{
    protected $table = "bio" ;

    public function Education() 
   {
      return $this->belongsTo('App\Models\Education');
   }

    public function job() 
   {
      return $this->belongsTo('App\Models\job');
   }

      public function training() 
   {
      return $this->belongsTo('App\Models\training');
   }


   
}
