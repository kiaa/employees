<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    
    protected $table = "job_experiences" ;

    public function Bio()
    {
    	return $this->hasMany('App\Models\Bio');
    }
}
